# -*- encoding: UTF-8 -*-
import cherrypy
from models import User, SecurityRules, LoginAttempt
from cgi import escape
import time
import logging
import pprint
from datetime import datetime
import json

logger = logging.getLogger()

#Dictionnary key for the session key
SESSION_KEY = '_cp_username'


def require(*conditions):
    """A decorator that appends conditions to the auth.require config
    variable."""
    def decorate(f):
        if not hasattr(f, '_cp_config'):
            f._cp_config = dict()
        if 'auth.require' not in f._cp_config:
            f._cp_config['auth.require'] = []
        f._cp_config['auth.require'].extend(conditions)
        return f
    return decorate


def member_of(groupname):
    """
        Return true if the current user is member
        of the specified group. Availaible groups
        are defined in models.py into the ROLES
        array.
    """
    def check():
        the_user = User.get_by_name(cherrypy.request.login)
        if the_user:
            return the_user.has_role(groupname)
        else:
            return False
    return check

# These might be handy
def any_of(*conditions):
    """Returns True if any of the conditions match"""
    def check():
        for c in conditions:
            if c():
                return True
        return False
    return check

def all_of(*conditions):
    """Returns True if all of the conditions match"""
    def check():
        for c in conditions:
            if not c():
                return False
        return True
    return check

# Controller to provide login and logout actions
class AuthController:

    ATTEMPTS = []

    def __index__(self):
        pass

    def on_login(self, username):
        """Called on successful login"""
        logger.info('Successful login from "%s"', username)
    
    def on_logout(self, username):
        """Called on logout"""
        logger.info('Successful logout from "%s"', username)
    
    def get_loginform(self, username, msg="", from_page="/index"):
        username = escape(username, True)
        from_page = escape(from_page, True)
        
        with open('./pages_html/auth.html', 'r', encoding='utf-8') as content_file:
            content = content_file.read()
            return content % locals()

    @cherrypy.expose
    def login(self, username=None, password=None, from_page="/"):
        if username is None or password is None:
            return self.get_loginform("", from_page=from_page)

        #Escape username
        username = escape(username, True)

        #Load security rules
        rules = SecurityRules.load()

        #Check if login attempt must be delayed
        failed_rounds = LoginAttempt.get_complete_round_of_failures(username, self.ATTEMPTS, rules.max_failed_login_attemps)
        if failed_rounds == 1:
            last_login = LoginAttempt.get_last_login_attempt(username, self.ATTEMPTS)
            minutes_since_last_attemps = int((datetime.now() - last_login.timestamp).total_seconds() / 60)
            print("minutes: " + str(minutes_since_last_attemps))
            if minutes_since_last_attemps < rules.delay_between_attempts_in_minutes: 
                error_msg = u'Trop de tentative échouées. Attendez quelques instants avant la prochaine tentative.'
                return self.get_loginform(username, error_msg, from_page) 

        #Get client ip
        client_ip = cherrypy.request.remote.ip

        #Log attempt
        attempt = LoginAttempt(username)
        self.ATTEMPTS.append(attempt)

        valid_login = False
        the_user = User.get_by_name(username)
        if the_user:
            valid_login, pwd_last_set_date = the_user.validate_credentials(password)

        #Invalid credentials
        if not valid_login:
            if not the_user:
                error_msg = u'Mauvais code d''utilisateur ou mot de passe'
                return self.get_loginform(username, error_msg, from_page)   

            #Check of abuse
            SecurityRules.check_for_login_abuse(attempt, self.ATTEMPTS)

            #Check for disabled account
            if not the_user.active:
                error_msg = u'Votre compte est désactivé. Contactez le support.'
                return self.get_loginform(username, error_msg)  

            #Send to pwd reset form if forced
            print("reset?" + str(the_user.force_reset))
            if the_user.force_reset:
                return self.get_pwd_reset_form(username)

            #Check if the the acount has been disabled
            if failed_rounds == 2:
                error_msg = u'Votre compte est désactivé. Contactez le support.'
                return self.get_loginform(username, error_msg, from_page) 

            error_msg = u'Mauvais code d''utilisateur ou mot de passe'
            return self.get_loginform(username, error_msg, from_page)   
        #Valid credentials
        else:
            #Always allow admin access from localhost to prevent DOS
            local_admin_request = the_user.has_role('Administrateur') and client_ip == '127.0.0.1'
            if local_admin_request:
                print("Local admin request")

            #Check for disabled account
            if not local_admin_request and not the_user.active:
                error_msg = u'Votre compte est désactivé. Contactez le support.'
                return self.get_loginform(username, error_msg)  

            #Generate session key
            cherrypy.session.regenerate()            
            cherrypy.request.login = username
            cherrypy.session[SESSION_KEY] = username

            #Log succesful login
            self.on_login(username)

            #Load security rules
            rules = SecurityRules.load()

            #Check if a password reset was forced by an admin
            forced_reset = the_user.force_reset

            #Check if the password is expired
            pwd_expired = (datetime.now() - pwd_last_set_date).days >= rules.pwd_expire_in_days

            filtered_attempts = LoginAttempt.clear_user_attempts(username, self.ATTEMPTS)
            self.ATTEMPTS = filtered_attempts or []

            if forced_reset or pwd_expired:
                return self.get_pwd_reset_form(username)
            else:
                #Send the user to the default page according to his role
                default_page = the_user.get_default_page_for_role()
                raise cherrypy.HTTPRedirect(default_page)

    @cherrypy.expose
    def logout(self, from_page="/"):
        sess = cherrypy.session
        username = sess.get(SESSION_KEY, None)
        sess[SESSION_KEY] = None
        if username:
            cherrypy.request.login = None
            self.on_logout(username)
        raise cherrypy.HTTPRedirect(from_page or "/")


    @cherrypy.expose
    @cherrypy.tools.allow(methods=['POST'])
    def update_password(self, username, old_pwd=None, new_pwd=None):
        username = escape(username, True)
        the_user = User.get_by_name(username)

        #Validate the password against current security rules
        rules = SecurityRules.load()
        valid_pwd = rules.validate_pwd(new_pwd, username)
        if not valid_pwd:
            return self.get_pwd_reset_form(username, 'Le mot de passe ne répond pas aux exigences de sécurité actuelles.')

        if the_user:
            #Log attempts
            attempt = LoginAttempt(username)
            self.ATTEMPTS.append(attempt)

            #Already used password ?
            if the_user.already_used_pwd(new_pwd):
                return self.get_pwd_reset_form(username,'La politique de sécurité ne permet pas de choisir deux fois le même mot de passe.') 

            #Old password or temporary password is correct ?
            valid, last_date = the_user.validate_credentials(old_pwd)
            if valid:
                the_user.set_active(True)
                the_user.update_pwd(new_pwd)
                the_user.force_reset = False
                User.update(the_user)
                filtered_attempts = LoginAttempt.clear_user_attempts(username, self.ATTEMPTS)
                self.ATTEMPTS = filtered_attempts or []
                return self.get_loginform(username,'Votre mot de passe a été mis à jour.')
            else:
                #Check of abuse
                rules = SecurityRules.load()
                SecurityRules.check_for_login_abuse(attempt, self.ATTEMPTS)

                #Check if the login must be delayed
                failed_rounds = LoginAttempt.get_complete_round_of_failures(username, self.ATTEMPTS, rules.max_failed_login_attemps)
                if failed_rounds == 1:
                    last_login = LoginAttempt.get_last_login_attempt(username, self.ATTEMPTS)
                    minutes_since_last_attemps = int((datetime.now() - last_login.timestamp).total_seconds() / 60)
                   
                    if minutes_since_last_attemps < rules.delay_between_attempts_in_minutes: 
                        error_msg = u'Trop de tentative échouées. Attendez quelques instants avant la prochaine tentative.'
                        return self.get_pwd_reset_form(username, error_msg)
                elif failed_rounds >= 2:



                    error_msg = u'Votre compte est désactivé. Contactez le support.'
                    return self.get_loginform(username, error_msg) 
                
            return self.get_pwd_reset_form(username,'Les informations d''authentication sont invalides.')
       
    @cherrypy.expose
    def get_pwd_reset_form(self, username=None, user_msg=''):
        with open('./pages_html/pwd_reset.html', 'r', encoding='utf-8') as content_file:
            msg = user_msg
            self.username = username
            content = content_file.read()
            return content % locals()



