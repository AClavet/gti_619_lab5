import hashlib
import uuid
import time
from datetime import datetime
from time import gmtime, strftime
import logging
logger = logging.getLogger()


class PwdMan:
	"""This class can read, write and update users passwords to a file.
	Passwords are hashed using sha512, salted with a unique guid and
	saved to pwd.dat using the following structure:

		user_id_x hashed_pwd_x salt_x date_time_x
		user_id_y hashed_pwd_y salt_y date_time_y
		... 

	The field user_id maps to User.user_id from the models.py module.

	The field date_time is used to know when that specific password was set. 
	"""

	def __init__(self):
		pass

	def write_pwd(self, original_pwd, user_id):
		#Protect the pwd
		salt, hashed_pwd = self._crypt_pwd(original_pwd)

		#Create file entry [ format: user_id hashed_pwd salt date_time_x ]
		file_entry = str(user_id) + ' ' + hashed_pwd + ' ' + salt + ' ' + strftime("%Y-%m-%d-%H:%M:%S") + '\n'

		#Append pwd entry to file with the hash and user_id
		with open('./app_data/pwd.dat' ,'a', encoding='utf-8') as outfile:
			outfile.write(file_entry)

		with open('./app_data/pwd_history.dat' ,'a', encoding='utf-8') as outfile:
			outfile.write(file_entry)


	def update_pwd(self, new_pwd, user_id):
		#Read original pwd file, ommiting the specified user
		lines = []
		with open('./app_data/pwd.dat' ,'r') as in_file:
			for line in in_file:
				values = line.rstrip().split(' ')
				file_user_id = values[0]

				if not str(user_id) == file_user_id:
					lines.append(line)

		#Write all lines back except the one for the specified user
		with open('./app_data/pwd.dat' ,'w') as out_file:
			for line in lines:
				out_file.write(line)

		#Write the new pwd down
		self.write_pwd(new_pwd, user_id)

	def get_already_used_pwd(self, user_id, original_pwd):
		"""Verifies if the proposed password is the same as the last one.

		Will also combine the proposed password with every salt 
		associated to that user. If we can manage to recreate 
		an existing password hash, then the password is already used.
		"""
		#Same password as last time?
		valid, last_update = self.validate_credentials(original_pwd, user_id)
		if valid:
			print('Trying to use the same password twice.')
			return True
		#New password
		else:
			#Get all salts and crypted passwords for this user
			salts = []
			hashed_pwds = []
			with open('./app_data/pwd_history.dat' ,'r', encoding='utf-8') as in_file:
				for line in in_file:
					values = line.rstrip().split(' ')
					file_user_id = values[0]
					if str(user_id) == file_user_id:
						file_hashed_pwd = values[1]
						salt = values[2]
						salts.append(salt)
						hashed_pwds.append(file_hashed_pwd)
			#Combine the provided password with each salt used to date
			for salt in salts:
				_salt, rebuilt_pwd = self._crypt_pwd(original_pwd, salt)
				if rebuilt_pwd in hashed_pwds:	
					#This password has already been used
					return True

			#The password is new
			return False

	def validate_credentials(self, pwd, user_id):
		"""Return true if the specified password match, otherwise false
		"""
		salt = None
		file_hashed_pwd = None
		pwd_date = None
		with open('./app_data/pwd.dat' ,'r', encoding='utf-8') as in_file:
			for line in in_file:
				values = line.rstrip().split(' ')
				file_user_id = values[0]

				if str(user_id) == file_user_id:
					file_hashed_pwd = values[1]
					salt = values[2]
					pwd_date = datetime.strptime(values[3], '%Y-%m-%d-%H:%M:%S')
					#print(salt, file_hashed_pwd)

		if salt and file_hashed_pwd:
			pwd = pwd.encode('utf-8')
			salt = salt.encode('utf-8')
			#Recreate pwd from original salt
			computed_hashed_pwd = hashlib.sha512(pwd + salt).hexdigest()
			#print(salt, computed_hashed_pwd)
			#Compare the computed hash to the one in the file
			if computed_hashed_pwd == file_hashed_pwd:
				print("The password matched")
				return True, pwd_date
			else:
				print("The password did not match")
		else:
			print("Could not find the specified user_id")

		return False, None

	def _crypt_pwd(self, original_pwd, _salt=None):
		#make sure the pwd is utf-8
		original_pwd = original_pwd.encode('utf-8')
		
		#Create salt
		salt = _salt or uuid.uuid4().hex
		salt_bytes = salt.encode('utf-8')

		#Compute the hash by appending the salt
		hashed_pwd = hashlib.sha512(original_pwd + salt_bytes).hexdigest()
	
		#Return salt and hash
		#print(salt, hashed_pwd)
		return salt, hashed_pwd


def test():
	pwd_man = PwdMan()
	#valid, pwd_date = pwd_man.validate_credentials('pwd', 1)
	#print(valid)
	#print(pwd_date)
	pwd_man.update_pwd('pwd', 1)
	pwd_man.update_pwd('pwd', 2)
	pwd_man.update_pwd('pwd', 3)
	#pwd_man.validate_credentials('pwd', 1)
	#pwd_man.update_pwd('My New password', 1)
	#pwd_man.validate_credentials('My New password', 1)
#test()