#Introduction

##Installer python sur Windows 7
Télécharger la version 3.3 de python et lancer .msi  
<http://www.python.org/ftp/python/3.3.3/python-3.3.3.msi>

!IMPORTANT!: Options d'installation -> Add python.exe to path
##Installer cherrypy
 
Télécharger cherrypy <https://pypi.python.org/pypi/CherryPy/3.2.4>, extraire l'archive et installer avec la commande suivante: 
```
python setup.py install
```

##Pour démarrer le serveur web:
```
python app.py
```
##Pour consulter le site, ouvrir un fureteur et aller à l'adresse suivante:
<https://localhost/>

##Les utilisateurs
1. Administrateur : pwd
2. Utilisateur 1  : pwd
3. Utilisateur 2  : pwd

##Les pages
###GET
<https://localhost/> ou <https://localhost:8080/index>   
<https://localhost/cercle>  
<https://localhost/carre>  
<https://localhost/admin>  
<https://localhost/auth/login>  
<https://localhost/auth/pwd_reset>  

###POST
<https://localhost/admin/update_password>  
<https://localhost/auth/login>  