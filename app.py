# -*- encoding: UTF-8 -*-

"""
C'est le script principal du système.

Pour démarrer un serveur web qui écoute sur le port 8080:

    python app.py

"""

import cherrypy
from models import SecurityRules, User, LoginAttempt
from auth import AuthController, require, member_of, any_of
import signal
import sys
from cgi import escape

SESSION_KEY = '_cp_username'

class AdminPage:
    _cp_config = {
        'auth.require': [member_of('Administrateur')]
    }
    @cherrypy.expose
    def index(self):
        return self.get_admin_page()

    def get_admin_page(self, user_msg=''):
        with open('./pages_html/admin.html', 'r', encoding='utf-8') as content_file:
            #Build HTML for user list
            user_list = '<select name="username">'
            for user in User.load_all_from_file():
                user_list = user_list + '<option value="%s">%s</option>' % (user.name, user.name)
            user_list = user_list + '</select>'
            content = content_file.read()

            #Load rules
            rules = SecurityRules.load()
            max_failed_login_attemps = rules.max_failed_login_attemps
            block_after_n_attempts = rules.block_after_n_attempts
            delay_between_attempts_in_minutes = rules.delay_between_attempts_in_minutes
            pwd_expire_in_days = rules.pwd_expire_in_days
            pwd_min_length = rules.pwd_min_length
            pwd_max_length = rules.pwd_max_length

            msg = user_msg
            #Render the page
            return content % locals()

    @cherrypy.expose
    @cherrypy.tools.allow(methods=['POST'])
    def reset_user_pwd(self, username, new_pwd=None, admin_pwd=None):
        username = escape(username, True)
        new_pwd = escape(new_pwd, True)
        admin_pwd = escape(admin_pwd, True)

        #Validate the password against current security rules
        rules = SecurityRules.load()
        valid_pwd = rules.validate_pwd(new_pwd, username)
        if not valid_pwd:
            return self.get_admin_page('Le mot de passe ne répond pas aux exigences de sécurité actuelles.')

        the_user = User.get_by_name(username)
        admin_user = User.get_by_name(cherrypy.request.login)

        if the_user and admin_user:
            valid_cred, last_update = User.validate_credentials(admin_user, admin_pwd)
            if valid_cred:
                the_user.set_active(True)
                the_user.update_pwd(new_pwd)
                the_user.force_reset = True
                User.update(the_user)
                return self.get_admin_page('Le mot de passe a été réinitialisé et le compte est réactivé.')
            else:
                return self.get_admin_page('Validez votre mot de passe.')
        else:
            return self.get_admin_page('Code d''utilisateur invalide')

    @cherrypy.expose
    @cherrypy.tools.allow(methods=['POST'])
    def update_security_rules(self, max_failed_login_attemps=None, block_after_n_attempts=None, delay_between_attempts_in_minutes=None, pwd_expire_in_days=None, pwd_min_length=None, pwd_max_length=None, admin_pwd=None):
        logging.info('Updating security rules...')

        admin_user = User.get_by_name(cherrypy.request.login)
        if not admin_user:
            return self.get_admin_page('Validez votre mot de passe.')

        valid_cred, last_update = User.validate_credentials(admin_user, admin_pwd)
        if not valid_cred:
            return self.get_admin_page('Validez votre mot de passe.')

        rules = SecurityRules()
        rules.max_failed_login_attemps = int(max_failed_login_attemps)
        rules.block_after_n_attempts = int(block_after_n_attempts)
        rules.delay_between_attempts_in_minutes = int(delay_between_attempts_in_minutes)
        rules.pwd_expire_in_days = int(pwd_expire_in_days) 
        rules.pwd_max_length = int(pwd_max_length)
        rules.pwd_min_length = int(pwd_min_length)
        SecurityRules.save(rules)  

        return self.get_admin_page('Les règles ont été mise à jour.')

class CirclePage:
    _cp_config = {
        'auth.require': [any_of(member_of('préposé au cercle'), member_of('Administrateur'))]
    }
    
    @cherrypy.expose
    def index(self):
        with open('./pages_html/cercle.html', 'r', encoding='utf-8') as content_file:
            content = content_file.read()
            return content % locals()

class SquarePage:
    _cp_config = {
        'auth.require': [any_of(member_of('préposé au carré'), member_of('Administrateur'))]
    }

    @cherrypy.expose
    def index(self):
        with open('./pages_html/carre.html', 'r', encoding='utf-8') as content_file:
            content = content_file.read()
            return content % locals()

class Root(object):
    _cp_config = {
        'tools.sessions.on': True,
        'tools.auth.on': True,
        'tools.sessions.timeout':1
    }

    #map to ./admin
    admin = AdminPage()

    #map to ./cercle
    cercle = CirclePage()

    #map to ./carre
    carre = SquarePage()

    #map to ./auth
    auth = AuthController()

    @cherrypy.expose
    @require()
    def index(self):
        with open('./pages_html/index.html', 'r', encoding='utf-8') as content_file:
            content = content_file.read()
            return content % locals()

def http_methods_allowed(methods=['GET', 'HEAD']):
    method = cherrypy.request.method.upper()
    if method not in methods:
        cherrypy.response.headers['Allow'] = ", ".join(methods)
        raise cherrypy.HTTPError(405)


def secureheaders():
    logging.debug('Injecting security headers...')
    headers = cherrypy.response.headers
    headers['X-Frame-Options'] = 'DENY'
    headers['X-XSS-Protection'] = '1; mode=block'
    headers['Content-Security-Policy'] = "default-src='self'"
    if (cherrypy.server.ssl_certificate != None and cherrypy.server.ssl_private_key != None):
        headers['Strict-Transport-Security'] = 'max-age=31536000' # one year

#Configure cherrypy
cherrypy.tools.allow = cherrypy.Tool('on_start_resource', http_methods_allowed)
config = {
    'environment': 'production',
    'log.screen' : False, #Log to screen and console
    'log.error_file': './app_data/log_webserver.log', #Where is the log file?
    'tools.sessions.on': True,
    'tools.sessions.storage_type': 'ram',
    'tools.sessions.secure': True,
    'tools.sessions.httponly' : True,
    'tools.secureheaders.on': True,
}

def check_auth(*args, **kwargs):
    """A tool that looks in config for 'auth.require'. If found and it
    is not None, a login is required and the entry is evaluated as a list of
    conditions that the user must fulfill"""
    if cherrypy.request.scheme == "http":
        secure_url = cherrypy.url().replace('http','https')
        raise cherrypy.HTTPRedirect(secure_url)

    conditions = cherrypy.request.config.get('auth.require', None)
    if conditions is not None:
        username = cherrypy.session.get(SESSION_KEY)
        if username:
            cherrypy.request.login = username
            for condition in conditions:
                # A condition is just a callable that returns true or false
                if not condition():
                    raise cherrypy.HTTPRedirect("/auth/login")
        else:
            raise cherrypy.HTTPRedirect("/auth/login")
    
cherrypy.tools.auth = cherrypy.Tool('before_handler', check_auth)

cherrypy.tools.secureheaders = cherrypy.Tool('before_finalize', secureheaders, priority=60)

#Configure python logger
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

#Stream writer
stream_handler = logging.StreamHandler()

#WFile writer
file_handler = logging.FileHandler('./app_data/log_application.log')
file_handler.setLevel(logging.DEBUG)

#Create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
stream_handler.setFormatter(formatter)

#Add the handlers to the logger
logger.addHandler(stream_handler)
logger.addHandler(file_handler)

#Bind to CTRL+C to exit gracefully
def signal_handler(signal, frame):
        cherrypy.engine.exit
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

#Start app
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--ssl_port', default=443)
    args = parser.parse_args()
    ssl_port = int(args.ssl_port)

    cherrypy.tree.mount(Root())
    cherrypy.server.unsubscribe()
 
    HTTPS_SERVER = cherrypy._cpserver.Server()
    HTTPS_SERVER.socket_port = ssl_port
    HTTPS_SERVER.ssl_certificate = './ssl/cert.pem'
    HTTPS_SERVER.ssl_private_key = './ssl/privkey.pem'
    HTTPS_SERVER.subscribe()
 
    HTTP_SERVER = cherrypy._cpserver.Server()
    HTTP_SERVER.socket_port = 80
    HTTP_SERVER.subscribe()
 
    cherrypy.config.update(config)
    cherrypy.engine.start()
    cherrypy.engine.block()