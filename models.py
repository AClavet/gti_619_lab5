# -*- encoding: UTF-8 -*-
from cgi import escape
from pwd_man import PwdMan
import json
from datetime import datetime
import logging
import math

#List of all roles
ROLES = ['Administrateur', 'préposé au cercle', 'préposé au carré']

logger = logging.getLogger()

class User:
	def __init__(self, user_id= 0, role='', name='', active=True, force_reset=False):
		self.user_id = user_id
		self.role = role
		self.name = name
		self.active = active #1 is active, 0 is inactive 
		self.force_reset = force_reset #True or false 

	def validate_credentials(self, pwd):
		return PwdMan().validate_credentials(pwd, self.user_id)

	def update_pwd(self, new_pwd):
		return PwdMan().update_pwd(new_pwd, self.user_id)

	def already_used_pwd(self, proposed_pwd):
		return PwdMan().get_already_used_pwd(self.user_id, proposed_pwd)
		
	def has_role(self, role):
		return self.role == role

	def has_name(self, name):
		return escape(self.name, True) == name

	def set_active(self, is_active):
		"""An inactive user must be given a new password.
		   It cannot login. (TODO)
		"""
		self.active = is_active

	def get_default_page_for_role(self):
		"""Return the default page for the specified role"""
		default_pages = {'Administrateur': '/admin', 'préposé au cercle': '/cercle', 'préposé au carré': '/carre'};
		return default_pages[self.role]

	def update(self):
		"""Save the current player state to file. The user must already exists
		"""
		#Read all users from users.dat
		users = User.load_all_from_file()

		users_without_self = []
		for user in users:
			if not user.user_id == self.user_id:
				users_without_self.append(user)

		#Write all lines back except the one for the specified user
		with open('./app_data/users.dat' ,'w') as out_file:
			for user in users_without_self:
				json.dump(user.__dict__, out_file)
				out_file.write("\n")

		#Append the user to users.dat
		with open('./app_data/users.dat' ,'a', encoding='utf-8') as out_file:
			json.dump(self.__dict__, out_file)
			out_file.write("\n")

	@staticmethod
	def get_by_name(username):
		"""Return the User object associated to the specified username.
		Return None if the user does not exists"""
		the_user = None
		for user in User.load_all_from_file():
			if user.name == username:
				print("Found user... " + user.name)
				the_user = user
				print(the_user.force_reset)
				break
		return the_user
	
	@staticmethod
	def load_all_from_file():
		"""Load all users from a file"""
		users = []
		with open('./app_data/users.dat', 'r') as users_file:
			for line in users_file:
				user = User()
				user.__dict__ = json.loads(line)
				users.append(user)	
		return users

	@staticmethod
	def reset_all():
		"""Reset users file"""
		USERS.append(User(1, ROLES[0], 'Administrateur'))
		USERS.append(User(2, ROLES[1], 'Utilisateur 1'))
		USERS.append(User(3, ROLES[2], 'Utilisateur 2'))

		with open('./app_data/users.dat' ,'w') as outfile:
			for user in USERS:
				json.dump(user.__dict__, outfile)
				outfile.write("\n")

class SecurityRules:
	"""This class defines security rules. 
	Rules are read from and written to ./app_data/security_rules.dat
	"""
	def __init__(self, max_failed_login_attemps=3, block_after_n_attempts=2, delay_between_attempts_in_minutes=1, pwd_expire_in_days=90):
		#After how many failed login attempts a security measure is applied?
		self.max_failed_login_attemps = max_failed_login_attemps
		#After n attempt, the account will be completely locked
		self.block_after_n_attempts = block_after_n_attempts
		#After one failed attempt, a delay of n minutes will be enforced between attemps
		self.delay_between_attempts_in_minutes = delay_between_attempts_in_minutes
		#Pwd expire after x days
		self.pwd_expire_in_days = pwd_expire_in_days
		#Pwd min length
		self.pwd_min_length = 6
		#Pwd max length
		self.pwd_max_length = 50

	def validate_pwd(self, pwd, username):
		"""Validate the specified password against the current rules"""
		if pwd == None:
			return False

		#Check length
		if len(pwd) < self.pwd_min_length or len(pwd) > self.pwd_max_length:
			return False

		#Make sure at least one letter is upper case
		one_upper = False
		for c in pwd:
			if c.istitle():
				one_upper = True
		if not one_upper:
			return False

		#Make sure it contains at least 1 non alphabetic character
		one_non_alpha = False
		for c in pwd:
			if not c.isalpha():
				one_non_alpha = True
		if not one_non_alpha:
			return False

		#Make sure the password is not the same as the username
		same_as_username = pwd == username
		if same_as_username:
			return False

		#Make sure no empty spaces are found
		contain_spaces = ' ' in pwd
		if contain_spaces:
			return False

		#The password is valid
		return True

	@staticmethod
	def load():
		with open('./app_data/security_rules.dat', 'r') as rules_file:
			for line in rules_file:
				rules = SecurityRules()
				rules.__dict__ = json.loads(line)
				return rules

	@staticmethod
	def save(security_rules):
		with open('./app_data/security_rules.dat' ,'w') as out_file:
			json.dump(security_rules.__dict__, out_file)
			out_file.write("\n")
	#Return true if an abuse is detected, otherwise return false.
	#TODO: Empêcher le denial of service pour un utilisateur légitime
	@staticmethod
	def check_for_login_abuse(attempt, attempts):
		#Log abuse verification for the specified user
		logger.info('Checking for login abuse from: ' + attempt.user)

		#Load security rules
		rules = SecurityRules.load()

		#Load user
		the_user = User.get_by_name(attempt.user)

		#Get failed attemps from this user
		user_attempts = LoginAttempt.get_user_attempts(attempt.user, attempts)

		complete_attemps_rounds = LoginAttempt.get_complete_round_of_failures(attempt.user, attempts, rules.max_failed_login_attemps)
		print(str(len(attempts)), complete_attemps_rounds)
		if complete_attemps_rounds == 1:
			logger.warn('Warning: %s is making a lot of failed login attemps', attempt.user)
		elif complete_attemps_rounds >= 2:
			the_user.set_active(False)
			logger.warn('Warning: %s account disabled. Too many attempts.', attempt.user)

			#Update user file
			User.update(the_user)
		else:
		    logger.info("%s is still below max_failed_login_attempts...", attempt.user)

class LoginAttempt:
    """Represents a login attempt"""
    def __init__(self, user):
        self.user = user
        self.timestamp = datetime.now()

    @staticmethod        
    def clear_user_attempts(user, attempts):
        filtered_attempts = []
        for attempt in attempts:
            if not attempt.user == user:
                filtered_attempts.append(attempt)
        return filtered_attempts

    @staticmethod        
    def get_user_attempts(user, attempts):
        filtered_attempts = []
        for attempt in attempts:
            if attempt.user == user:
                filtered_attempts.append(attempt)
        return filtered_attempts

    @staticmethod
    def get_last_login_attempt(user, attempts):
        user_attempts = LoginAttempt.get_user_attempts(user, attempts)
        if len(user_attempts) > 0:
            return user_attempts[-1] # [-1] Returns the last item from the list
        else:
            return None

    @staticmethod
    def get_complete_round_of_failures(user, attempts, round_size):
        user_attempts = LoginAttempt.get_user_attempts(user, attempts)

        return math.floor(len(user_attempts) / round_size)	
#SecurityRules.save(SecurityRules())
#User.reset_all()